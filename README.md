# Interview test task into IT company #

### Brief task description ###
* Client - Server applications that communicates over TCP protocol (som set of commands). Server should has ability to perist it's state (model's state) between restarts.
For more details about task pleases take a look at FullTask.pdf file.

### Task state at deadline ###
First version with all the requirements implemented was compleated!

### Used technicues and technologies ###
* Java8
* JPA/Hibernate (above SQLite to simplify deployment)
* RAII
* TCP Socket IPC
* Design patterns
* ... and much more (please glance through modules dependencies in pom.xml files)

### Project structure ###
* Server module
* Client module
* ProtocolLibrary module -- contains all the stuff that is used by both Client and Server (like constants, command names and other protocol-agnostic details...)
* IntegrationTests module -- contains different kinds of tests (mainly unit tests, integration tests and aux demo application for dev testsing)

### How to test? ###
* Just run Server and Client jars from the Release directory and enjoy (use --help option for man) ;)
* Also you can run Test jar to show demo working in stand-alone mode!

### Ways for improvement ###
Well... This is a good topic for discussion later.
For the while we have first version with the full functionality according specification!
Important note is that we have already had a lot of tests that will carefully verify our changes during future improvements ;)