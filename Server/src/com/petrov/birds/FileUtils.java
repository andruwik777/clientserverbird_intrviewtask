package com.petrov.birds;

import java.io.File;

/**
 * Some aux methods for Server module
 * @author petrov
 *
 */
public class FileUtils {

	/**
	 * Creates directory
	 * 
	 * @param outDir
	 *            -- output directory
	 * @return true if directory exists and can write permissions
	 */
	public static boolean createDir(String outDir) {
		File dir = new File(outDir);
		dir.mkdir();
		return dir.exists() && dir.canWrite();
	}
}
