package com.petrov.birds;

import java.io.File;
import java.io.IOException;

import com.beust.jcommander.IValueValidator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.petrov.birds.dao.BirdsDao;
import com.petrov.birds.dao.DaoException;

/**
 * Entry point of Server module
 * @author petrov
 *
 */
public class ServerMain implements AutoCloseable {
	@Parameter(names = { "-" + Constants.PORT }, description = "Server port (default is " + Constants.DEFAULT_PORT
			+ ")", validateValueWith = PortValidator.class)
	private Integer serverPort = 3000;

	@Parameter(names = { "-"
			+ Constants.DATA }, description = "Folder where server keeps its data store", validateValueWith = OutDirValidator.class)
	private String outDir = System.getProperty("user.home") + File.separator + "serverdata";

	@Parameter(names = { "-"
			+ Constants.PROC_COUNT }, description = "Number of workers which process different requests from clients.", validateValueWith = ThreadPoolSizeValidator.class)
	private Integer workerThreadsPoolSize = 2;

	@Parameter(names = "--help", help = true)
	private boolean isHelpChosen = false;

	private NetworkListener networkListener;

	// usually this will be in detached state. Only external thread will merge
	// all changes in db using flush method...
	private BirdsDao birdsDao;

	private Thread daoTimeoutFlushThread;

	/** 
	 * Entry point
	 * @param args
	 * @throws IOException
	 */
	public ServerMain(String[] args) throws IOException {
		parseArgs(args);
		birdsDao = new BirdsDao(outDir);
		networkListener = new NetworkListener(serverPort, birdsDao, workerThreadsPoolSize);
		daoTimeoutFlushThread = createDaoTimeoutFlushThread();
	}

	private Thread createDaoTimeoutFlushThread() {
		return new Thread(() -> {

			try {
				while (true) {
					Thread.sleep(Constants.DAO_FLUSHING_TIMEOUT);
					birdsDao.flush();
				}
			} catch (InterruptedException | DaoException e) {
				Loger.Server.log("Server's timeoutFlushThread was terminated!");
			}
		});
	}

	public static void main(String[] args) {
		Loger.Server.log("START!");
		try (ServerMain serverMain = new ServerMain(args);) {
			serverMain.performUserDesire();
		} catch (IOException e) {
			Loger.Server.logError(e, "SHUTDOWN...");
		}
		Loger.Server.log("FINISH!");
	}

	/**
	 * Throws exception in case of some error or user wants to exit (check
	 * exception message for {@link Constants#QUIT} equality)
	 * 
	 * @throws IOException
	 */
	private void performUserDesire() throws IOException {
		startDaoTimeoutFlushThread();
		networkListener.listen();
	}

	private void parseArgs(String[] args) {
		// String[] commandLineArgs = { "-data", "cccc" };
		// String[] commandLineArgs = { "--help" };
		String[] commandLineArgs = args;
		JCommander jCommander = new JCommander(this, commandLineArgs);
		jCommander.setProgramName("Server");

		if (isHelpChosen) {
			jCommander.usage();
			return;
		}
	}

	/**
	 * Port validator
	 * @author andrey
	 *
	 */
	public static class PortValidator implements IValueValidator<Integer> {

		public void validate(String name, Integer value) throws ParameterException {
			if (name.equals("-" + Constants.PORT)) {
				if (value < Constants.MIN_PORT_VALUE || value > Constants.MAX_PORT_VALUE) {
					throw new ParameterException("A valid TCP port number is between 1 and 65535");
				}
			}
		}
	}

	/**
	 * Out dir validator
	 * @author andrey
	 *
	 */
	public static class OutDirValidator implements IValueValidator<String> {

		public void validate(String name, String value) throws ParameterException {
			if (name.equals("-" + Constants.DATA)) {
				if (!FileUtils.createDir(value)) {
					throw new ParameterException("Can't create our dir: " + value);
				}
			}
		}
	}

	/**
	 * Thread pool size validator
	 * @author andrey
	 *
	 */
	public static class ThreadPoolSizeValidator implements IValueValidator<Integer> {

		public void validate(String name, Integer value) throws ParameterException {
			if (name.equals("-" + Constants.PROC_COUNT)) {
				if (value < Constants.MIN_THREAD_POOL_SIZE_VALUE || value > Constants.MAX_THREAD_POOL_SIZE_VALUE) {
					throw new ParameterException("A valid thread size pool should be between "
							+ Constants.MIN_THREAD_POOL_SIZE_VALUE + " and " + Constants.MAX_THREAD_POOL_SIZE_VALUE);
				}
			}
		}
	}

	// Autoclosable interface implementation -- terminates thread
	@Override
	public void close() {
		terminateDaoTimeoutFlushThread();
	}
	
	private void startDaoTimeoutFlushThread() {
		daoTimeoutFlushThread.start();
	}

	private void terminateDaoTimeoutFlushThread() {
		daoTimeoutFlushThread.interrupt();
		try {
			daoTimeoutFlushThread.join();
		} catch (InterruptedException e) {
			Loger.Server.logError(e, "Can't join to daoTimeoutFlushThread");
		}
	}

}
