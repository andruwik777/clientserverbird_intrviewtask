package com.petrov.birds;

/** 
 * Server-specific exception
 * @author petrov
 *
 */
public class ServerException extends RuntimeException {
	public ServerException() {
	}

	public ServerException(String message) {
		super(message);
	}

	public ServerException(String message, Exception e) {
		super(message, e);
	}
}
