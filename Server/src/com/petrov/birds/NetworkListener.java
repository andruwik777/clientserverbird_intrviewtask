package com.petrov.birds;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.gson.Gson;
import com.petrov.birds.dao.BirdsDao;
import com.petrov.birds.dao.DaoException;
import com.petrov.birds.models.Bird;
import com.petrov.birds.models.Sighting;
import com.petrov.birds.requests.AddBirdRequest;
import com.petrov.birds.requests.AddSightingRequest;
import com.petrov.birds.requests.BaseRequest;
import com.petrov.birds.requests.ListSightingsRequest;
import com.petrov.birds.requests.RemoveBirdRequest;
import com.petrov.birds.responses.BaseResponse;
import com.petrov.birds.responses.ListBirdsResponse;
import com.petrov.birds.responses.ListSightingsResponse;

/**
 * Listens for requests and respond with corresponding response
 * <p> Also see inner {@link WorkerThread} implementation
 * @author petrov
 *
 */
public class NetworkListener {
	private ServerSocket serverSocket;
	private Gson gson = Utils.getGson();
	private BirdsDao birdsDao;
	private ExecutorService executor;
	private Thread daoShutdownFlushHook;

	public NetworkListener(Integer port, BirdsDao birdsDao, int workerThreadsCount) throws IOException {
		serverSocket = new ServerSocket(port);
		this.birdsDao = birdsDao;
		executor = Executors.newFixedThreadPool(workerThreadsCount);
		daoShutdownFlushHook = new Thread(()->{
			try {
				birdsDao.flush();
			} catch (DaoException e) {
				Loger.Server.logError(e, "Flushing at shutdown hook failed");
			}
		});
		Runtime.getRuntime().addShutdownHook(daoShutdownFlushHook);
	}

	/**
	 * Listen for client's connections
	 */
	public void listen() {
		Loger.Server.log("Started listen at port " + serverSocket.getLocalPort());

		Socket clientSocket;
		try {
			while (true) {
				clientSocket = serverSocket.accept();
				executor.execute(new WorkerThread(clientSocket));
			}
		} catch (IOException e) {
			Loger.Server.log("Finishing listening...");
			executor.shutdown();
		}

	}

	private void terminateServer() {
		try {
			serverSocket.close();
		} catch (IOException e) {
			Loger.Server.logError(e, "Can't close serverSocket");
		}
	}

	/**
	 * Process corresponding client's request 
	 * @author petrov
	 *
	 */
	class WorkerThread implements Runnable {
		private Socket clientSocket;

		public WorkerThread(Socket socket) {
			clientSocket = socket;
		}

		@Override
		public void run() {
			try {
				processClient();
			} catch (ServerException e) {
				if (Constants.QUIT.equals(e.getMessage())) {
					Loger.Server.log("Client ask to quit. SHUTDOWN...");
					Runtime.getRuntime().removeShutdownHook(daoShutdownFlushHook);
					terminateServer();
				} else {
					Loger.Server.logError(e, "Worker thread error!");
				}
			} catch (IOException e) {
				Loger.Server.logError(e, "Worker thread error!");
			} finally {
				try {
					clientSocket.close();
				} catch (IOException e) {
					Loger.Server.logError(e, "Can't close client Socket!");
				}
			}
		}

		private void processClient() throws IOException {
			BaseResponse response;
			boolean needQuit = false;
			try (InputStream is = clientSocket.getInputStream(); OutputStream os = clientSocket.getOutputStream()) {
				String stringRequest = Utils.readStringFromStream(is);
				BaseRequest baseRequest = gson.fromJson(stringRequest, BaseRequest.class);
				try {
					switch (baseRequest.getCommand()) {
					case Constants.ADD_BIRD: {
						AddBirdRequest request = gson.fromJson(stringRequest, AddBirdRequest.class);
						birdsDao.addBird(request.getBird());
						response = new BaseResponse(true,
								"Bird was added successfully! Request: " + gson.toJson(request.getBird()));
						break;
					}
					case Constants.ADD_SIGHTING: {
						AddSightingRequest request = gson.fromJson(stringRequest, AddSightingRequest.class);
						birdsDao.addSighting(request.getSighting());
						response = new BaseResponse(true,
								"Sighting was added successfully! Request: " + gson.toJson(request.getSighting()));
						break;
					}
					case Constants.LIST_BIRDS: {
						Set<Bird> birds = birdsDao.getBirds();
						response = new ListBirdsResponse(true,
								"Birds was fetched successfully! Request: " + gson.toJson(baseRequest), birds);
						break;
					}
					case Constants.LIST_SIGHTINGS: {
						ListSightingsRequest request = gson.fromJson(stringRequest, ListSightingsRequest.class);
						Set<Sighting> sightings = birdsDao.getSightings(request.getName(), request.getStartDate(),
								request.getEndDate());
						response = new ListSightingsResponse(true,
								"Sightings was fetched successfully! Request: " + gson.toJson(request), sightings);
						break;
					}
					case Constants.REMOVE_BIRD:
						RemoveBirdRequest request = gson.fromJson(stringRequest, RemoveBirdRequest.class);
						birdsDao.removeBird(request.getName());
						response = new BaseResponse(true,
								"Bird was removed successfully! Request: " + gson.toJson(request));
						break;
					case Constants.QUIT:
						needQuit = true;
						birdsDao.flush();
						response = new BaseResponse(true, "Server will shotdown soon...");
						break;
					default:
						String errorMessage = "Unknown command: " + baseRequest.getCommand();
						Loger.Server.logError(errorMessage);
						throw new ServerException(errorMessage);
					}

				} catch (DaoException e) {
					response = new BaseResponse(false, e.getMessage());
				}

				Utils.writeStringToStream(os, gson.toJson(response));
			}

			if (needQuit) {
				throw new ServerException(Constants.QUIT);
			}
		}
	}
}
