package com.petrov.birds.dao;

/**
 * DAO specific exception
 * @author petrov
 *
 */
public class DaoException extends Exception {
	public DaoException() {
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(String message, Exception e) {
		super(message, e);
	}
}
