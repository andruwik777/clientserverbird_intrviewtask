package com.petrov.birds.dao;

import java.io.File;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.google.common.collect.Sets;
import com.petrov.birds.Loger;
import com.petrov.birds.models.Bird;
import com.petrov.birds.models.Sighting;

/**
 * Provides DAO for {@link Bird} and {@link Sighting} entities
 * 
 * @author petrov
 *
 */
public class BirdsDao {
	private static final String SQLITE_DB_NAME = "birds.sqlite";

	private final Set<Bird> birds;
	private final String outDir;
	private final ReentrantReadWriteLock rwLock;

	private SessionFactory configureSessionFactory() throws HibernateException {
		Configuration configuration = new Configuration().configure();
		configuration.setProperty("hibernate.connection.url",
				"jdbc:sqlite:" + outDir + File.separator + SQLITE_DB_NAME);
		ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		return sessionFactory;
	}

	/**
	 * Construct DAO object
	 * 
	 * @param outDir
	 */
	@SuppressWarnings("unchecked")
	public BirdsDao(String outDir) {
		rwLock = new ReentrantReadWriteLock();
		this.outDir = outDir;
		SessionFactory sessionFactory = configureSessionFactory();
		try {
			Session session = sessionFactory.openSession();
			try {
				birds = ConcurrentHashMap.newKeySet();
				birds.addAll((session.createCriteria(Bird.class).list()));
			} finally {
				session.close();
			}

		} finally {
			sessionFactory.close();
		}
	}

	/**
	 * Get Birds
	 * 
	 * @return Set<Bird>
	 * @throws DaoException
	 */
	public Set<Bird> getBirds() throws DaoException {
		return birds;
	}

	/**
	 * Add bird
	 * 
	 * @param bird
	 * @throws DaoException
	 */
	public void addBird(Bird bird) throws DaoException {
		try {
			rwLock.readLock().lock();
			if (!birds.add(bird)) {
				throw new DaoException("Birds container already contains bird with the same name!");
			}
		} finally {
			rwLock.readLock().unlock();
		}
	}

	/**
	 * Server uses Java regular expression library to match the appropriate bird
	 * names and uses the date range to find the appropriate entries.
	 * <p>
	 * Server sends this data to the client and client prints a table with
	 * following columns: name, date which is sorted by alphabetical order of
	 * the bird names and further sorted in order of time.
	 */
	public Set<Sighting> getSightings(String birdNameRegex, Long startDate, Long endDate) throws DaoException {
		Set<Bird> birds = getBirds(birdNameRegex);
		Set<Sighting> sightings = birds.stream().map(Bird::getSightings).flatMap(Set::stream)
				.collect(Collectors.toSet());
		Predicate<Sighting> predicate = sighting -> isDateInRange(sighting.getTimestamp(), startDate, endDate);
		Set<Sighting> result = sightings.stream().filter(predicate).sorted((left, right) -> {
			int nameComparison = left.getBird().getName().compareTo(right.getBird().getName());
			return nameComparison != 0 ? nameComparison : left.getTimestamp().compareTo(right.getTimestamp());
		}).collect(Collectors.toSet());
		return result;
	}

	private boolean isDateInRange(Long date, Long startDate, Long endDate) {
		return date >= startDate && date <= endDate;
	}

	/**
	 * Add sighting
	 * 
	 * @param sighting
	 * @throws DaoException
	 */
	public void addSighting(Sighting sighting) throws DaoException {
		try {
			rwLock.readLock().lock();

			Bird b = getBird(sighting.getBird().getName());
			sighting.setBird(b);
			if (!b.getSightings().add(sighting)) {
				throw new DaoException("Such bird sighting has already been stored for this bird!");
			}
		} finally {
			rwLock.readLock().unlock();
		}
	}

	/**
	 * Removes bird
	 * 
	 * @param birdName
	 * @throws DaoException
	 */
	public void removeBird(String birdName) throws DaoException {
		try {
			rwLock.readLock().lock();

			Bird bird = getBird(birdName);
			if (!birds.remove(bird)) {
				throw new DaoException("No such bird in continer!");
			}
		} finally {
			rwLock.readLock().unlock();
		}
	}

	private Bird getBird(String birdName) {
		Predicate<Bird> predicate = bird -> bird.getName().equals(birdName);
		Bird bird = birds.stream().filter(predicate).findFirst().get();
		return bird;
	}

	private Set<Bird> getBirds(String birdNameRegex) {
		Predicate<Bird> predicate = bird -> Pattern.matches(birdNameRegex, bird.getName());
		Set<Bird> result = birds.stream().filter(predicate).collect(Collectors.toSet());
		return result;
	}

	/**
	 * Sync local in-memory model with database (in other words it saves this
	 * in-memory model to DB).
	 * 
	 * @throws ParseException
	 */
	public void flush() throws DaoException {
		try {
			rwLock.writeLock().lock();

			SessionFactory sessionFactory = configureSessionFactory();
			try {
				Session session = sessionFactory.openSession();
				try {
					org.hibernate.Transaction tr = session.beginTransaction();
					try {
						saveNewElements(session);
						removeDeletedElements(session);
						tr.commit();
					} catch (RuntimeException e) {
						tr.rollback();
						Loger.Server.logError(e, "Flushing failed!");
						throw e;
					}
				} finally {
					session.close();
				}
			} finally {
				sessionFactory.close();
			}
		} finally {
			rwLock.writeLock().unlock();
		}
	}

	private void removeDeletedElements(Session session) {
		Set<Bird> dbBirds = new HashSet<>(session.createCriteria(Bird.class).list());
		Set<Bird> removed = Sets.symmetricDifference(dbBirds, birds);
		birds.removeAll(removed);
		removed.forEach(b -> session.delete(b));
	}

	private void saveNewElements(Session session) {
		for (Bird b : birds) {
			Set<Sighting> sightings = b.getSightings();
			for (Sighting s : sightings) {
				session.saveOrUpdate(s);
			}

			session.saveOrUpdate(b);
		}
	}
}
