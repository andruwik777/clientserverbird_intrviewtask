package com.petrov.birds;

/**
 * Constants that either:
 * <p> * are used with both Client and Server]
 * <p> * or just for simplifying project configuration
 * @author petrov
 *
 */
public class Constants {
	public static final String SERVER_PORT = "serverPort";
	public static final String PORT = "port";

	public static final String ADD_BIRD = "addbird";
	public static final String ADD_SIGHTING = "addsighting";
	public static final String LIST_BIRDS = "listbirds";
	public static final String LIST_SIGHTINGS = "listsightings";
	public static final String REMOVE_BIRD = "remove";
	public static final String QUIT = "quit";

	public static final String DATA = "data";
	public static final String PROC_COUNT = "proc_count";

	public static final String DEFAULT_PORT = "3000";
	public static final Integer MIN_PORT_VALUE = 1;
	public static final Integer MAX_PORT_VALUE = 65535;
	public static final Integer MIN_THREAD_POOL_SIZE_VALUE = 1;
	public static final Integer MAX_THREAD_POOL_SIZE_VALUE = 500;
	public static final String CHARSET = "UTF-8";

	public static final String DATE_FORMAT = "yyyy-MM-dd";
	public static final String TIME_FORMAT = "HH:mm:ss";
	public static final String FULL_TIMESTAMP_FORMAT = DATE_FORMAT + "_" + TIME_FORMAT;
	
	public static final long DAO_FLUSHING_TIMEOUT = 60000;
}
