package com.petrov.birds;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.petrov.birds.models.Bird;
import com.petrov.birds.models.Sighting;
import com.petrov.birds.responses.BaseResponse;
import com.petrov.birds.responses.ListBirdsResponse;
import com.petrov.birds.responses.ListSightingsResponse;

import de.vandermeer.asciitable.AsciiTable;

/**
 * This is just a simple logger for Hello World proj
 * Don't use in production (use Log4j for example..)
 * @author petrov
 *
 */
public class Loger {
	public static void log(String s) {
		System.out.println(s);
	}

	public static class Server {
		public static void log(String s) {
			System.out.println("SERVER -> " + s);
		}

		public static void log(Exception e) {
			logError(e.getMessage());
		}

		public static void logError(String message) {
			log("ERROR:" + message);
		}

		public static void logError(Exception e, String message) {
			logError(message);
			log(e);
		}
	}

	public static class Client {
		public static void log(String s) {
			System.out.println("CLIENT-> " + s);
		}

		public static void log(Exception e) {
			log("ERROR:" + e.getMessage());
		}

		public static void log(BaseResponse response) {
			log((response.getIsSuccessful() ? "SUCCESS! " : "ERROR! ") + response.getMessage());
		}

		public static void log(ListBirdsResponse response) {
			log((BaseResponse) response);
			log("Total number of birds is " + response.getBirds().size());

			if (response.getIsSuccessful()) {
				AsciiTable at = new AsciiTable();

				at.addRule();
				at.addRow("NAME", "COLOR", "HEIGHT", "WEIGHT");
				for (Bird bird : response.getBirds()) {
					at.addRule();
					at.addRow(bird.getName(), bird.getColor(), bird.getHeight(), bird.getWeight());
				}
				at.addRule();

				String rend = at.render();
				log("\r\n" + rend);
			}
		}

		public static void log(ListSightingsResponse response) {
			log((BaseResponse) response);
			log("Total number of sightings is " + response.getSightings().size());

			if (response.getIsSuccessful()) {
				AsciiTable at = new AsciiTable();

				at.addRule();
				at.addRow("NAME", "LOCATION", "DATE", "TIME");
				for (Sighting sighting : response.getSightings()) {
					at.addRule();

					Date date = new Date(sighting.getTimestamp());
					SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
					String dateString = dateFormat.format(date);

					SimpleDateFormat timeFormat = new SimpleDateFormat(Constants.TIME_FORMAT);
					String timeString = timeFormat.format(date);

					at.addRow(sighting.getBird().getName(), sighting.getLocation(), dateString, timeString);
				}
				at.addRule();

				String rend = at.render();
				log("\r\n" + rend);
			}
		}
	}

}
