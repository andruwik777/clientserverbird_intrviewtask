package com.petrov.birds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/** 
 * Contains some specific utils methods for library or client/server module
 * @author petrov
 *
 */
public class Utils {

	/**
	 * Return Gson instance 
	 * @return
	 */
	public static Gson getGson() {
		return new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	}

	/**
	 * Read string from stream
	 * @param is
	 * @return
	 * @throws IOException
	 */
	public static String readStringFromStream(InputStream is) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(is));
		return in.readLine();
	}

	/**
	 * Write string to stream
	 * @param os
	 * @param data
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 */
	public static void writeStringToStream(OutputStream os, String data)
			throws UnsupportedEncodingException, IOException {
		PrintWriter out = new PrintWriter(os, true);
		out.println(data);
	}
}
