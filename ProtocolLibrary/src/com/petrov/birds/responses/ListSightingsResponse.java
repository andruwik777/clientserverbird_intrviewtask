package com.petrov.birds.responses;

import java.util.Set;

import com.google.gson.annotations.Expose;
import com.petrov.birds.models.Sighting;;

/* 
 * Representation of ListSightings response
 */
public class ListSightingsResponse extends BaseResponse {
	@Expose
	private Set<Sighting> sightings;

	public ListSightingsResponse(Boolean isSuccessful, String message, Set<Sighting> sightings) {
		super(isSuccessful, message);
		this.sightings = sightings;
	}

	public Set<Sighting> getSightings() {
		return sightings;
	}

}
