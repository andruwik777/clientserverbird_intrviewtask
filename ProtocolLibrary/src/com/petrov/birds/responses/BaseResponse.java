package com.petrov.birds.responses;

import com.google.gson.annotations.Expose;

/*
 * Parent class for all the responses that provide common info about whether command was processed successfully 
 */
public class BaseResponse {
	@Expose
	private final Boolean isSuccessful;
	@Expose
	private final String message;

	public BaseResponse(Boolean isSuccessful, String message) {
		this.isSuccessful = isSuccessful;
		this.message = message;
	}

	public Boolean getIsSuccessful() {
		return isSuccessful;
	}

	public String getMessage() {
		return message;
	}
}
