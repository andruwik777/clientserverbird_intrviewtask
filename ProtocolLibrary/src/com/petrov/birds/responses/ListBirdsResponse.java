package com.petrov.birds.responses;

import java.util.Set;

import com.google.gson.annotations.Expose;
import com.petrov.birds.models.Bird;

/* 
 * Representation of ListBirds response
 */
public class ListBirdsResponse extends BaseResponse {
	@Expose
	private final Set<Bird> birds;

	public ListBirdsResponse(Boolean isSuccessful, String messages, Set<Bird> birds2) {
		super(isSuccessful, messages);
		this.birds = birds2;
	}

	public Set<Bird> getBirds() {
		return birds;
	}

}
