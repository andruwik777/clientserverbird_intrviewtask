package com.petrov.birds.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.google.gson.annotations.Expose;

/* 
 * Representation of Bird entity 
 */
@Entity
public class Bird implements java.io.Serializable {
	@Expose
	@Id
	private String name;
	@Expose
	private String color;
	@Expose
	private Integer weight;
	@Expose
	private Integer height;

	@OneToMany(mappedBy = "bird", fetch = FetchType.EAGER, cascade = { CascadeType.ALL, CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	private Set<Sighting> sightings = new HashSet<>();

	public Bird() {
	}
	
	@Override
	public int hashCode() {
		int result = 17;
        result = 31 * result + name.hashCode();
        return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (!(obj instanceof Bird))
	        return false;

		return name.equals(((Bird)obj).name);
	}

	public Bird(String name, String color, Integer weight, Integer height) {
		this.name = name;
		this.color = color;
		this.weight = weight;
		this.height = height;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Set<Sighting> getSightings() {
		return sightings;
	}

	public void setSightings(Set<Sighting> sightings) {
		this.sightings = sightings;
	}
}
