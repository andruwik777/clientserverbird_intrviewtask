package com.petrov.birds.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.Expose;

/* 
 * Representation of Sighting entity 
 */
@Entity
public class Sighting  implements java.io.Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer sightingId;

	@Expose
	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL, CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE })
	@JoinColumn(name = "name")
	private Bird bird;

	@Expose
	private String location;
	
	@Expose
	private Long timestamp;
	
	public Sighting(){
	}

	public Sighting(Bird bird, String location, Long timestamp) {
		this.bird = bird;
		this.location = location;
		this.timestamp = timestamp;
	}

	@Override
	public int hashCode() {
		int result = 17;
        result = 31 * result + bird.hashCode();
        result = 31 * result + timestamp.hashCode();
        return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (!(obj instanceof Sighting))
	        return false;

	    Sighting sighting = (Sighting)obj;
		return bird.equals(sighting.bird) && timestamp.equals(sighting.timestamp);
	}

	public Integer getSightingId() {
		return sightingId;
	}

	public void setSightingId(Integer sightingId) {
		this.sightingId = sightingId;
	}

	public Bird getBird() {
		return bird;
	}

	public void setBird(Bird bird) {
		this.bird = bird;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	

}
