package com.petrov.birds.requests;

import com.google.gson.annotations.Expose;

/* 
 * Representation of ListSightings request
 */
public class ListSightingsRequest extends BaseRequest {
	@Expose
	private final String name;
	@Expose
	private final Long startDate;
	@Expose
	private final Long endDate;

	public ListSightingsRequest(String command, String name, Long startDate, Long endDate) {
		super(command);
		this.name = name;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public Long getStartDate() {
		return startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

}
