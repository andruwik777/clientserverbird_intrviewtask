package com.petrov.birds.requests;

import com.google.gson.annotations.Expose;
import com.petrov.birds.models.Bird;

/* 
 * Representation of AddBird request
 */
public class AddBirdRequest extends BaseRequest {
	@Expose
	private final Bird bird;

	public AddBirdRequest(String command, Bird bird) {
		super(command);
		this.bird = bird;
	}

	public Bird getBird() {
		return bird;
	}

}
