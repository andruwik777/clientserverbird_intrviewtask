package com.petrov.birds.requests;

public class ListBirdsRequest extends BaseRequest {

	/* 
	 * Representation of ListBirds request
	 */
	public ListBirdsRequest(String command) {
		super(command);
	}

}