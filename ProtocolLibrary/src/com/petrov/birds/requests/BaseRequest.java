package com.petrov.birds.requests;

import com.google.gson.annotations.Expose;
import com.petrov.birds.Constants;

/**
 * Base class that specifies one of the available server commands.
 * <p> See constant values in {@link Constants} class
 */
public class BaseRequest {
	
	/* package-level access to allow children to use this field without redundant getter method
	 * 
	 */
	@Expose
	String command;

	public BaseRequest(String command) {
		this.command = command;
	}

	public String getCommand() {
		return command;
	}

}
