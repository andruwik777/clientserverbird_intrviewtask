package com.petrov.birds.requests;

/**
 * Representation of Quit request
 *
 */
public class QuitRequest extends BaseRequest {

	public QuitRequest(String command) {
		super(command);
	}
}
