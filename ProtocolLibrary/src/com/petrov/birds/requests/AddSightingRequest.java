package com.petrov.birds.requests;

import com.google.gson.annotations.Expose;
import com.petrov.birds.models.Sighting;

/* 
 * Representation of AddSighting request
 */
public class AddSightingRequest extends BaseRequest {
	@Expose
	private final Sighting sighting;

	public AddSightingRequest(String command, Sighting sighting) {
		super(command);
		this.sighting = sighting;
	}

	public Sighting getSighting() {
		return sighting;
	}

}
