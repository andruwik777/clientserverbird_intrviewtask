package com.petrov.birds.requests;

import com.google.gson.annotations.Expose;

/* 
 * Representation of RemoveBird request
 */
public class RemoveBirdRequest extends BaseRequest {
	@Expose
	private final String name;

	public RemoveBirdRequest(String command, String name) {
		super(command);
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
