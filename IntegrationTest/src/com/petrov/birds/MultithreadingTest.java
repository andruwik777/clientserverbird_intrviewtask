package com.petrov.birds;

import static org.apache.commons.io.FileUtils.deleteDirectory;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.petrov.birds.TestMain.ServerThread;
import com.petrov.birds.dao.BirdsDao;
import com.petrov.birds.dao.DaoException;
import com.petrov.birds.models.Bird;

public class MultithreadingTest {
	private static final String TEST_DIR = "MULTI_TMP_TEST_DIR_MULTI";
	BirdsDao dao;

	@Before
	public void setup() throws IOException {
		deleteDirectory(new File(TEST_DIR));

		FileUtils.createDir(TEST_DIR);
		dao = new BirdsDao(TEST_DIR);
	}

	@Test
	public void testMultithreading() throws InterruptedException, DaoException {
		ServerThread serverThread = new ServerThread();
		serverThread.start();

		Thread.sleep(3000);

		final Thread quitCommandThread = new Thread(() -> {
			TestMain.callClientMain(Arrays.asList("-quit"));
		});

		IntStream.range(1, 100).forEachOrdered(n -> {
			if (n == 99) {
				quitCommandThread.start();
				return;
			}

			Thread t = new Thread(() -> {
				if (n % 10 == 0) {
					TestMain.callClientMain(Arrays.asList("-remove", "-name", "bird" + (n - 5)));
				} else {
					TestMain.callClientMain(Arrays.asList("-addbird", "-name", "bird" + n, "-color", "color" + n,
							"-weight", "" + n, "-height", "" + n));
				}
			});
			t.start();
		});
		
		serverThread.join();
		
		BirdsDao birdsDao = new BirdsDao(TEST_DIR);
		Set<Bird> birds = birdsDao.getBirds();
		Assert.assertTrue(birds.size() > 0);
	}
}
