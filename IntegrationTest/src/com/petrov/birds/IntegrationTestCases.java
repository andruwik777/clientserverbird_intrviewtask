package com.petrov.birds;

import static com.petrov.birds.TestMain.callClientMain;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IntegrationTestCases {
	private static final String TEST_DIR = "TMP_TEST_DIR";
	private Thread serverThread;

	@Before
	public void setup() throws IOException, InterruptedException {
		org.apache.commons.io.FileUtils.deleteDirectory(new File(TEST_DIR));
		serverThread = new Thread(new Runnable() {
			@Override
			public void run() {
				String[] args = { "-data", TEST_DIR };
				ServerMain.main(args);
			}
		});
		serverThread.start();
		Thread.sleep(3000);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void quitServerTestCase() throws InterruptedException {
		org.junit.Assert.assertTrue(serverThread.isAlive());

		callClientMain(Arrays.asList("-quit"));
		Thread.sleep(3000);

		Assert.assertFalse(serverThread.isAlive());
	}
}
