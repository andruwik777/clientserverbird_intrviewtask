package com.petrov.birds;

import java.util.Arrays;
import java.util.List;

public class TestMain {
	public static void main(String[] args) {
		Loger.log("TEST START!");

		try {
			Thread serverThread = new ServerThread();
			serverThread.start();

			// server loading...
			Thread.sleep(3000);

			Thread clientThread = new ClientThread();
			clientThread.start();

			clientThread.join();
			serverThread.join();
		} catch (InterruptedException e) {
			Loger.log("TEST ERROR:" + e.getMessage());
		}

		Loger.log("TEST FINISH!");
	}

	public static void callClientMain(List<String> args) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			Loger.log("ERROR:" + e.getMessage());
		}
		ClientMain.main(args.stream().toArray(String[]::new));
	}

	static class ServerThread extends Thread {
		@Override
		public void run() {
			String[] args = { "-data", "AAA_OUT_TEST_DIR_AAA" };
			ServerMain.main(args);
		}
	}

	private static class ClientThread extends Thread {
		@Override
		public void run() {

			callClientMain(
					Arrays.asList("-addbird", "-name", "bird1", "-color", "green1", "-weight", "11", "-height", "12"));

			callClientMain(
					Arrays.asList("-addbird", "-name", "bird2", "-color", "green2", "-weight", "21", "-height", "22"));

			callClientMain(
					Arrays.asList("-addbird", "-name", "bird3", "-color", "green3", "-weight", "31", "-height", "31"));

			callClientMain(Arrays.asList("-addbird", "-name", "bird3", "-color", "green333", "-weight", "333",
					"-height", "333"));

			callClientMain(Arrays.asList("-listbirds"));

			callClientMain(Arrays.asList("-addsighting", "-name", "bird1", "-location", "location11", "-date",
					"2017-09-09_09:11:00"));

			callClientMain(Arrays.asList("-addsighting", "-name", "bird2", "-location", "location21", "-date",
					"2017-09-09_09:21:00"));

			callClientMain(Arrays.asList("-addsighting", "-name", "bird2", "-location", "location22", "-date",
					"2017-09-09_09:22:00"));

			callClientMain(Arrays.asList("-addsighting", "-name", "bird3", "-location", "location31", "-date",
					"2017-09-09_09:31:00"));

			callClientMain(Arrays.asList("-addsighting", "-name", "bird3", "-location", "location32", "-date",
					"2017-09-09_09:32:00"));

			callClientMain(Arrays.asList("-addsighting", "-name", "bird3", "-location", "location333", "-date",
					"2017-09-09_09:32:00"));

			callClientMain(Arrays.asList("-listsightings", "-name", "bird[1|3]", "-startDate", "2017-09-09_09:11:00",
					"-endDate", "2017-09-09_09:31:30"));

			callClientMain(Arrays.asList("-remove", "-name", "bird3"));

			callClientMain(Arrays.asList("-listbirds"));

			callClientMain(Arrays.asList("-listsightings", "-name", "bird.*", "-startDate", "2017-09-09_09:10:00",
					"-endDate", "2017-09-09_21:31:30"));

			callClientMain(Arrays.asList("-quit"));
		}
	}
}
