package com.petrov.birds;

import static org.apache.commons.io.FileUtils.deleteDirectory;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.petrov.birds.dao.BirdsDao;
import com.petrov.birds.dao.DaoException;
import com.petrov.birds.models.Bird;
import com.petrov.birds.models.Sighting;;

public class DaoUnitTest {
	private static final String TEST_DIR = "AAA_TMP_TEST_DIR_AAA";
	BirdsDao dao;

	@Before
	public void setup() throws IOException {
		deleteDirectory(new File(TEST_DIR));

		FileUtils.createDir(TEST_DIR);
		dao = new BirdsDao(TEST_DIR);
	}

	@Test
	public void addBirdsTest() throws DaoException {
		dao.addBird(new Bird("1", "11", 111, 1111));
		dao.addBird(new Bird("2", "22", 222, 2222));
		dao.flush();

		BirdsDao newDao = new BirdsDao(TEST_DIR);
		Set<Bird> birds = newDao.getBirds();

		Assert.assertEquals(2, birds.size());
	}

	@Test
	public void addSightingTest() throws DaoException, ParseException {
		{
			dao.addBird(new Bird("1", "11", 111, 1111));
			dao.flush();
		}
		{
			BirdsDao newDao = new BirdsDao(TEST_DIR);
			Bird b = newDao.getBirds().iterator().next();
			Long timestamp1 = new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:11:00")
					.getTime();
			Long timestamp2 = new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:12:00")
					.getTime();
			newDao.addSighting(new Sighting(b, "location1", timestamp1));
			newDao.addSighting(new Sighting(b, "location2", timestamp2));
			newDao.flush();
		}
		{
			BirdsDao newDao2 = new BirdsDao(TEST_DIR);
			Set<Bird> birds = newDao2.getBirds();
			Set<Sighting> sightings = birds.iterator().next().getSightings();

			Assert.assertEquals(2, sightings.size());
		}
	}

	@Test
	public void addSightingTest2() throws DaoException, ParseException {
		{
			Bird bird = new Bird("1", "11", 111, 1111);
			dao.addBird(bird);
			dao.flush();
		}

		{
			BirdsDao newDao2 = new BirdsDao(TEST_DIR);
			Set<Bird> birds = newDao2.getBirds();
			Bird b = birds.iterator().next();
			Long timestamp1 = new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:11:00")
					.getTime();
			b.getSightings().add(new Sighting(b, "location1", timestamp1));
			newDao2.flush();
		}

		{
			BirdsDao newDao3 = new BirdsDao(TEST_DIR);
			Set<Bird> birds = newDao3.getBirds();
			Bird b = birds.iterator().next();
			Assert.assertEquals(1, b.getSightings().size());
		}
	}
	
	@Test(expected = DaoException.class)
	public void addExistingSightingTest() throws DaoException, ParseException {
		{
			dao.addBird(new Bird("1", "11", 111, 1111));
			dao.flush();
		}
		{
			BirdsDao newDao = new BirdsDao(TEST_DIR);
			Bird b = newDao.getBirds().iterator().next();
			Long timestamp1 = new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:11:00")
					.getTime();
			newDao.addSighting(new Sighting(b, "location1", timestamp1));
			newDao.addSighting(new Sighting(b, "location2", timestamp1));
		}
	}

	@Test
	public void twoFlushesTest() throws DaoException {
		dao.addBird(new Bird("1", "11", 111, 1111));
		dao.addBird(new Bird("2", "22", 222, 2222));
		dao.flush();
		dao.flush();

		BirdsDao newDao = new BirdsDao(TEST_DIR);
		Set<Bird> birds = newDao.getBirds();

		Assert.assertEquals(2, birds.size());
	}

	@Test(expected = DaoException.class)
	public void addExistingBirdsTest() throws DaoException {
		dao.addBird(new Bird("1", "11", 111, 1111));
		dao.addBird(new Bird("1", "22", 333, 4444));
	}

	@Test
	public void removeBirdTest() throws DaoException {
		dao.addBird(new Bird("1", "11", 111, 1111));
		dao.addBird(new Bird("2", "22", 222, 2222));
		dao.addBird(new Bird("3", "33", 333, 3333));
		dao.flush();

		BirdsDao newDao = new BirdsDao(TEST_DIR);
		Set<Bird> birds = newDao.getBirds();
		Assert.assertEquals(3, birds.size());
		newDao.removeBird("2");
		newDao.flush();

		BirdsDao newDao2 = new BirdsDao(TEST_DIR);
		Set<Bird> birds2 = newDao2.getBirds();

		Assert.assertEquals(2, birds2.size());
	}
	
	@Test
	public void removeBirdWithSightingTest() throws DaoException, ParseException {
		Bird bb = new Bird("1", "11", 111, 1111);
		dao.addBird(bb);
		dao.addSighting(new Sighting(bb, "loc1", new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:11:00").getTime()));
		Bird bb2 = new Bird("2", "22", 222, 2222);
		dao.addBird(bb2);
		dao.addSighting(new Sighting(bb2, "loc2", new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:12:00").getTime()));
		dao.flush();

		BirdsDao newDao = new BirdsDao(TEST_DIR);
		Set<Bird> birds = newDao.getBirds();
		Assert.assertEquals(2, birds.size());
		Assert.assertEquals(1, birds.iterator().next().getSightings().size());
		
		Set<Sighting> ss = newDao.getSightings(".*", new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:10:00").getTime(), new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:12:00").getTime());
		Assert.assertEquals(2, ss.size());
		
		
		newDao.removeBird("1");
		newDao.flush();

		BirdsDao newDao2 = new BirdsDao(TEST_DIR);
		Set<Sighting> ss2 = newDao2.getSightings(".*", new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:10:00").getTime(), new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse("2017-09-09_09:12:00").getTime());
		Assert.assertEquals(1, ss2.size());
	}

	/**
	 * Emulating of devtesting error
	 * <p> A different object with the same identifier value was already associated with the session : [com.petrov.birds.models.Bird#bird1]
	 * @throws DaoException
	 * @throws ParseException
	 */
	@Test
	public void addBirdAndSightingTest() throws DaoException {

		//callClientMain(Arrays.asList("-addbird", "-name", "bird1", "-color", "green1", "-weight", "11", "-height", "12"));
		Bird bird = new Bird("bird1", "green1", 11, 12);
		dao.addBird(bird);
		
		// ("-addsighting", "-name", "bird1", "-location", "location11", "-date", "2017-09-09_09:11:00"));
		Bird tmp = new Bird("bird1", null, null, null);
		dao.addSighting(new Sighting(tmp, "location11", 1504937460000l));
		dao.flush();
		
		// no throwing detected!
		Assert.assertTrue(true);
	}
}
