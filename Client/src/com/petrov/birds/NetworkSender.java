package com.petrov.birds;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.petrov.birds.requests.AddBirdRequest;
import com.petrov.birds.requests.AddSightingRequest;
import com.petrov.birds.requests.BaseRequest;
import com.petrov.birds.requests.ListBirdsRequest;
import com.petrov.birds.requests.ListSightingsRequest;
import com.petrov.birds.requests.QuitRequest;
import com.petrov.birds.requests.RemoveBirdRequest;
import com.petrov.birds.responses.BaseResponse;
import com.petrov.birds.responses.ListBirdsResponse;
import com.petrov.birds.responses.ListSightingsResponse;

/** 
 * Sends client's request and process corresponding response
 * @author petrov
 */
public class NetworkSender {
	private final Gson gson = Utils.getGson();
	private final Integer port;

	public NetworkSender(Integer port) {
		this.port = port;
	}

	/**
	 * Send ListBirdsRequest to server and obtain response
	 * @param request
	 * @return
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	public ListBirdsResponse send(ListBirdsRequest request) throws JsonSyntaxException, IOException {
		return gson.fromJson(getResponseAsString(request), ListBirdsResponse.class);
	}

	/**
	 * Send ListSightingsResponse to server and obtain response
	 * @param request
	 * @return
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	public ListSightingsResponse send(ListSightingsRequest request) throws JsonSyntaxException, IOException {
		return gson.fromJson(getResponseAsString(request), ListSightingsResponse.class);
	}

	/**
	 * Send AddBirdRequest to server and obtain response
	 * @param request
	 * @return
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	public BaseResponse send(AddBirdRequest request) throws JsonSyntaxException, IOException {
		return gson.fromJson(getResponseAsString(request), BaseResponse.class);
	}

	/**
	 * Send AddSightingRequest to server and obtain response
	 * @param request
	 * @return
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	public BaseResponse send(AddSightingRequest request) throws JsonSyntaxException, IOException {
		return gson.fromJson(getResponseAsString(request), BaseResponse.class);
	}

	/**
	 * Send RemoveBirdRequest to server and obtain response
	 * @param request
	 * @return
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	public BaseResponse send(RemoveBirdRequest request) throws JsonSyntaxException, IOException {
		return gson.fromJson(getResponseAsString(request), BaseResponse.class);
	}

	/**
	 * Send QuitRequest to server and obtain response
	 * @param request
	 * @return
	 * @throws JsonSyntaxException
	 * @throws IOException
	 */
	public BaseResponse send(QuitRequest request) throws JsonSyntaxException, IOException {
		return gson.fromJson(getResponseAsString(request), BaseResponse.class);
	};

	private String getResponseAsString(BaseRequest request) throws IOException {
		String stringRequest = gson.toJson(request);
		try (Socket socket = new Socket("localhost", port);
				OutputStream os = socket.getOutputStream();
				InputStream is = socket.getInputStream();) {
			Utils.writeStringToStream(os, stringRequest);
			return Utils.readStringFromStream(is);

		}
	}
}
