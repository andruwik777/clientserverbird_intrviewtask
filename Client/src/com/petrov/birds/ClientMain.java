package com.petrov.birds;

import static com.petrov.birds.Loger.Client.log;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import com.beust.jcommander.IValueValidator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.google.gson.JsonSyntaxException;
import com.petrov.birds.models.Bird;
import com.petrov.birds.models.Sighting;
import com.petrov.birds.requests.AddBirdRequest;
import com.petrov.birds.requests.AddSightingRequest;
import com.petrov.birds.requests.ListBirdsRequest;
import com.petrov.birds.requests.ListSightingsRequest;
import com.petrov.birds.requests.QuitRequest;
import com.petrov.birds.requests.RemoveBirdRequest;
import com.petrov.birds.responses.BaseResponse;
import com.petrov.birds.responses.ListBirdsResponse;
import com.petrov.birds.responses.ListSightingsResponse;

/**
 * Client's entry point
 * @author petrov
 *
 */
public class ClientMain {
	@Parameter(names = { "-" + Constants.SERVER_PORT }, description = "Server port (default is "
			+ Constants.DEFAULT_PORT + ")", validateValueWith = PortValidator.class)
	private Integer serverPort = 3000;

	@Parameter(names = { "-" + Constants.ADD_BIRD }, description = "Add bird command")
	private boolean isAddBirdChosen = false;

	@Parameter(names = { "-name" }, description = "Bird name")
	private String name = null;

	@Parameter(names = { "-" + Constants.ADD_SIGHTING }, description = "Add sighting command")
	private boolean isAddSightingChosen = false;

	@Parameter(names = { "-" + Constants.LIST_BIRDS }, description = "List birds command")
	private boolean isListBirdsChosen = false;

	@Parameter(names = { "-" + Constants.LIST_SIGHTINGS }, description = "List sightings command")
	private boolean isListSightingsChosen = false;

	@Parameter(names = { "-" + Constants.REMOVE_BIRD }, description = "Remove bird command")
	private boolean isRemoveBirdChosen = false;

	@Parameter(names = { "-" + Constants.QUIT }, description = "Quit command")
	private boolean isQuitChosen = false;

	// Other aux parameters
	///////////////////////////////////////////
	@Parameter(names = { "-color" }, description = "Bird color")
	private String color = null;

	@Parameter(names = { "-weight" }, description = "Bird weight")
	private Integer weight = null;

	@Parameter(names = { "-height" }, description = "Bird height")
	private Integer height = null;

	@Parameter(names = { "-date" }, description = "Sighting date in format " + Constants.FULL_TIMESTAMP_FORMAT)
	private String date = null;

	@Parameter(names = { "-location" }, description = "Sighting location")
	private String location = null;

	@Parameter(names = { "-startDate" }, description = "Sighting start date")
	private String startDate = null;

	@Parameter(names = { "-endDate" }, description = "Sighting start date")
	private String endDate = null;

	///////////////////////////////////////////

	@Parameter(names = "--help", help = true)
	private boolean isHelpChosen = false;

	/**
	 * Entry point
	 * @param args
	 */
	public static void main(String[] args) {
		log("START! : " + Arrays.toString(args));
		try {
			ClientMain clientMain = new ClientMain();
			clientMain.parseArgs(args);
			clientMain.performUserDesire();
		} catch (ParameterException e) {
			Loger.Client.log(e.getMessage());
		} catch (JsonSyntaxException | IOException | ParseException e) {
			Loger.Client.log(e);
		}

		log("FINISH!");
	}

	private void performUserDesire() throws JsonSyntaxException, IOException, ParseException {
		NetworkSender networkSender = new NetworkSender(serverPort);

		if (isAddBirdChosen) {
			processAddBird(networkSender, new Bird(name, color, weight, height));
		}
		if (isAddSightingChosen) {
			Long timestamp = new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse(date).getTime();

			processAddSightings(networkSender, new Sighting(new Bird(name, null, null, null), location, timestamp));
		}
		if (isListBirdsChosen) {
			processLisrtBirds(networkSender);
		}
		if (isListSightingsChosen) {
			Long startTimestamp = new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse(startDate).getTime();
			Long endTimestamp = new SimpleDateFormat(Constants.FULL_TIMESTAMP_FORMAT).parse(endDate).getTime();
			processListSightings(networkSender, name, startTimestamp, endTimestamp);
		}
		if (isRemoveBirdChosen) {
			processRemoveBird(networkSender, name);
		}
		if (isQuitChosen) {
			processQuit(networkSender);
		}
	}

	private static void processQuit(NetworkSender networkSender) throws JsonSyntaxException, IOException {
		showResult(networkSender.send(new QuitRequest(Constants.QUIT)));
	}

	private static void processRemoveBird(NetworkSender networkSender, String name) throws JsonSyntaxException, IOException {
		showResult(networkSender.send(new RemoveBirdRequest(Constants.REMOVE_BIRD, name)));
	}

	private static void processListSightings(NetworkSender networkSender, String name, Long startDate, Long endDate) throws JsonSyntaxException, IOException {
		showResult(networkSender.send(new ListSightingsRequest(Constants.LIST_SIGHTINGS, name, startDate, endDate)));
	}

	private static void processLisrtBirds(NetworkSender networkSender) throws JsonSyntaxException, IOException {
		showResult(networkSender.send(new ListBirdsRequest(Constants.LIST_BIRDS)));
	}

	private static void processAddSightings(NetworkSender networkSender, Sighting sighting) throws JsonSyntaxException, IOException {
		showResult(networkSender.send(new AddSightingRequest(Constants.ADD_SIGHTING, sighting)));
	}

	private static void processAddBird(NetworkSender networkSender, Bird bird) throws JsonSyntaxException, IOException {
		showResult(networkSender.send(new AddBirdRequest(Constants.ADD_BIRD, bird)));
	}

	private static void showResult(BaseResponse response) {
		Loger.Client.log(response);
	}

	private static void showResult(ListBirdsResponse response) {
		Loger.Client.log(response);
	}

	private static void showResult(ListSightingsResponse response) {
		Loger.Client.log(response);
	}

	private void parseArgs(String[] args) {
		// String[] commandLineArgs = { "-quit" };
		// String[] commandLineArgs = { "--help" };
		String[] commandLineArgs = args;
		JCommander jCommander = new JCommander(this, commandLineArgs);
		jCommander.setProgramName("Client");

		if (isHelpChosen) {
			jCommander.usage();
			return;
		}

		validateExclusiveParameters();
	}

	private void validateExclusiveParameters() {
		int exclusiveParametersCout = 0;
		if (isAddBirdChosen) {
			if (name == null || color == null || weight == null || height == null || date != null || location != null
					|| startDate != null || endDate != null) {
				throw new ParameterException(
						"For [-addbird] command you must additionally specify only [-name][-color][-weight][-height]. I.e. -addbird -name parrot -color red -weight 85 -height 98");
			}
			++exclusiveParametersCout;
		}
		if (isAddSightingChosen) {
			if (name == null || color != null || weight != null || height != null || date == null || location == null
					|| startDate != null || endDate != null) {
				throw new ParameterException(
						"For [-addsighting] command you must additionally specify only [-name][-location][-date]. Date format must be next: "
								+ Constants.FULL_TIMESTAMP_FORMAT + ". I.e. -addsighting -name parrot -location California -date 2017-09-17_09:11:00");
			}
			++exclusiveParametersCout;
		}
		if (isListBirdsChosen) {
			if (name != null || color != null || weight != null || height != null || date != null || location != null
					|| startDate != null || endDate != null) {
				throw new ParameterException("For [-listbirds] command you must not specify any of additional commands!");
			}
			++exclusiveParametersCout;
		}
		if (isListSightingsChosen) {
			if (name == null || color != null || weight != null || height != null || date != null || location != null
					|| startDate == null || endDate == null) {
				throw new ParameterException(
						"For [-listsightings] command you must additionally specify only [-name][-startDate][-endDate]. Date format must be next: "
								+ Constants.FULL_TIMESTAMP_FORMAT + ". I.e. -listsightings -name parrot -startDate 2017-09-17_09:11:00 -endDate 2017-09-18_09:11:00");
			}
			++exclusiveParametersCout;
		}
		if (isRemoveBirdChosen) {
			if (name == null || color != null || weight != null || height != null || date != null || location != null
					|| startDate != null || endDate != null) {
				throw new ParameterException("For [-remove] command you must additionally specify only [-name]. I.e. -remove -name parrot");
			}
			++exclusiveParametersCout;
		}
		if (isQuitChosen) {
			if (name != null || color != null || weight != null || height != null || date != null || location != null
					|| startDate != null || endDate != null) {
				throw new ParameterException("For [-quit] command you must not specify any of additional commands!");
			}
			++exclusiveParametersCout;
		}
		
		if (exclusiveParametersCout == 0) {
			throw new ParameterException(
					"Please specify one of the following commands: -addbird, -addsighting, -listbirds, -listsightings, -remove, -quit.");
		}else if(exclusiveParametersCout > 1)  {
			throw new ParameterException(
					"-addbird, -addsighting, -listbirds, -listsightings, -remove and -quit are exclusing commands. Only one of them can be provided on the command line at the time.");
		}
	}

	/**
	 * Validate port of user input
	 * @author andrey
	 *
	 */
	public static class PortValidator implements IValueValidator<Integer> {

		public void validate(String name, Integer value) throws ParameterException {
			if (name.equals("-" + Constants.SERVER_PORT)) {
				if (value < Constants.MIN_PORT_VALUE || value > Constants.MAX_PORT_VALUE) {
					throw new ParameterException("A valid TCP port number is between " + Constants.MIN_PORT_VALUE
							+ " and " + Constants.MAX_PORT_VALUE);
				}
			}
		}
	}
}
